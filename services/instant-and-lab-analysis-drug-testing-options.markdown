---
layout: services
title:  "Instant & Lab Analysis Drug Testing Options"
group: services
---
Benefits of instant drug testing:

- Results within hours for negative results
- Increased worker productivity: reduced testing time allows for placement of employees into productive positions
- Reduced employment costs: decrease absenteeism due to substance abuse
- Shortened hiring cycle: eliminates 2-3 day delay in hiring prospective employees
- Reduced drug testing costs: lower "cost per test" because we have our own on-site lab for instant results

Instant Drug Testing Services include:

- Rapid Results – within hours for negative results
- All non-negative results are laboratory verified, within 2-3 working days and reviewed by a Certified MRO (Medical Review Officer)
- Proven laboratory accuracy
- FDA approved
- 5 Panel Test: Cocaine Metabolites, Marijuana Metabolities, Phenyclidine (PCP, "Angel Dust") , Amphetamines, Methamphetamines ("Speed"), Opiates (Codeine, Morphine, Heroin)

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->