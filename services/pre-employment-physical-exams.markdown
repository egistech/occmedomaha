---
layout: services
title:  "Pre-Employment Physical Exams"
group: services
---
<!--
Photo needs to be of an examiner with a patient (maybe vision testing?)
-->

Pre-Employment Physical Examinations are performed to determine if an applicant is physically capable of meeting the essential functions of the job, while maintaining the examinee's privacy and medical confidentiality. A well designed physical exam program results in fewer injuries, lower costs and a better match of employees to their jobs.

The exam is performed with attention to the job duties, including physical requirements and potential exposures to hazardous materials. The content of the examination depends on the job and the work-site assessment. For example, jobs that require heavy lifting may require a back assessment to determine if the employee is able to perform the functions of the job. Those involved in interstate trucking usually require a physical exam and a urinary drug test.

To avoid errors in either the content or the context of the examination, Family OccMed of Omaha uses standard protocols to which the employer and the examining provider both agree. Following the examination, the medical provider gives a written opinion about the person's suitability to perform the job to ensure maximized productivity while reducing the potential for injury to themselves or others.

### A Medical Examination Typically Includes:

Family OccMed of Omaha providers will carefully review your medical and occupational history before proceeding with the physical examination.

- Complete occupational/medical history
- Musculoskeletal examination
- Evaluation of the respiratory system
- Gastrointestinal examination
- Examination of head, eyes, ears, nose, and throat
- Skin and lymphatic examination
- Neurological evaluation
- Evaluation of the cardiovascular system
- Visual acuity and vital signs, including blood pressure, urine dip test
- For a convenient, walk-in pre-employment physical, contact Family OccMed of Omaha.

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->