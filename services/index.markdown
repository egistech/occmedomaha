---
layout: services
title: "Services"
group: services
---
### What is Occupational Medicine?

Occupational Medicine is most simply, the area where companies, employee’s health and medicine overlap. Family OccMed of Omaha is concerned with protecting the safety and health of today’s worker’s, and the financial health of our client customers.

We commit to taking excellent care of your employees, whether they are injured, need a physical, need drug testing or our urgent care services.Without great medical care and user-friendly testing, employees will be unhappy, complications will occur, treatment will be prolonged and medical costs will soar.

Increase productivity and reduce medical expenses:our doctors focus on excellence in delivery, providing prompt and high quality care and testing.This level of service helps injured employees recover and those that need testing to comply with industry standards quickly, which allows them to get back to work faster and easier. Your organization will realize enhanced productivity as well as a healthy bottom line with the services and approach used at Family OccMed of Omaha.

![Checkin](http://assets.occmedomaha.com/images/inside/IMG-1138R.jpg)

***

As experts in occupational medicine, we help your employees who are sick or injured on the job achieve the best possible recovery by:

- Determining if their illness or injury is work related.
- Identifying appropriate medical tests.
- Developing specialized treatment plans.
- Assisting in scheduling rehabilitation services.

We can also help you prevent your employees from becoming sick or injured on the job by:

- Recommending safe work place practices and operations.
- Assisting in developing health and safety programs.
- Ensuring your workers return to work safely by identifying alternative, “light-duty” and work restriction programs.