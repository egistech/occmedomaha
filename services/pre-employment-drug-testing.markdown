---
layout: services
title:  "Pre-Employment Drug Testing"
group: services
---
A company with over 15 employees is subject to the **Americans with Disabilities Act**, a federal law that protects people with disabilities from discrimination. This Act prohibits pre-employment medical examinations before a conditional offer of employment has been made but a test to determine if an applicant is illegally using drugs is specifically exempted from the definition of what constitutes a medical examination. However pre-employment alcohol testing is considered a medical examination and can only be conducted after the candidate has been extended a conditional offer of employment.

*Discrimination can be implied if an employer tests only certain applicants for a position. An employer cannot pick and choose which applicants for the same position will be tested. However within a company employment drug testing may be required for only certain positions. The company would have to be able to justify this in terms of the job requirements and a written drug policy.*

### Why Do Pre-Employment Drug Testing?

The US Department of Labor has estimated that drug use in the workplace costs employers up to $100 billion dollars annually in lost work time, accidents, health care costs and workers compensation costs.

**Drug use affects your bottom line.** Workers who do drugs are more likely to change jobs or miss work. The good news is you can put programs in place to make sure your workplace is drug free. A good drug free program includes testing applicants prior to their hire date and performing random drug testing throughout their tenure with your company.

### What are Requirements for Pre-Employment Drug Testing ?

Generally these requirements for pre-employment drug screening are followed by employers:

- Employers have a written drug testing policy that requires job applicants to be drug-free.
- Written notice of testing is given before the applicant may be tested. Many drug and alcohol testing laws require that job applicants be notified in advance that they may be tested and under what conditions.
- Written notice is given to the applicant that employment drug screening is required before hiring. This may be done through the employment application form or on a specific form given out at the first interview.
- The written notice details the type of drug testing that will be carried out and lists the over-the-counter medications that may produce a positive result.
- The same testing program should be implemented for all applicants in a particular category or there could be implications of discrimination.

<!--
Photo – better photo of outside of clinic

Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->