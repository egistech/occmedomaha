---
layout: services
title:  "Workman's Comp"
group: services
---
Family OccMed of Omaha provides treatment for work related illness and injuries, post-accident drug and/or alcohol testing and on-going monitoring of an employee's progress during their recovery process. We work closely with employers to provide appropriate restrictions that allow Companies to provide alternative work duties, including "light duty".

### Workplace Illness or Injury:

When an employee presents for care, we will need authorization from you [LINK TO AUTHORIZATION FORM] that you are aware of this illness or injury and the patient is a valid employee of yours. The employee you send needs to know the name and number of the person at your company to call for us to talk to.

When we call you, some of the questions we will ask are:

- For verbal authorization of care
- Work comp insurance carrier and Policy number 
  - We need to know this in order to file the claim properly without incurring more expenses.
- Do you want the employee tested for drugs or alcohol?
  - Family OccMed of Omaha has the ability to test the employee for drugs and/or alcohol at the time of treatment. To streamline the process, please let us know as soon as possible that a post-accident drug and/or alcohol test is needed.

If you have sent an employee to us and they are on their way, you can speed up the process by giving us a call at ______________ / _________________ / __________________ so that we can authorize the information and begin the treatment process as soon as the patient arrives.

##### Coordination of Care

Family OccMed of Omaha provides on-going monitoring of an employee's progress during their recovery process. We work closely with physical therapists, orthopaedic physicians, and other healthcare modalities to facilitate the best possible outcome for your employee.

<!--
ADD LINK TO THE FORM ABOUT WORKERS' COMP
-->