---
layout: services
title:  "Breath Alcohol Testing"
group: services
---
### How Does Breath Alcohol Testing Work?

Anyone who consumes alcoholic beverages will exhale alcohol from their lungs. The alcohol concentration in the breath is in direct proportion to the concentration of alcohol in the blood that is making its way to the person's brain. The use of an Evidential Breath Tester (EBT) which measures the concentration of alcohol in a person's breath is the preferred method for testing alcohol levels in the body as it is a less invasive process than blood tests.

<!--
[[[FIND/TAKE A PICTURE OF OUR BREATH ALCOHOL TEST!!]]]
-->

### Alcohol Concentration in the Body and Impairment

- At 0.01-0.05 the ability to make good judgments become impaired.
- At 0.03-0.12 Sensory-motor control is impaired and slowed information processing is experiences.
- At 0.09-0.25 Reaction time is decreased, vision functions are reduced, drowsiness is experienced.
- At 0.09-0.25 Loss of critical judging abilities, impairment of perception, memory and comprehension is experienced.
- 0.18-0.30 Metal confusion, exaggerated emotional states, incoordination, slurred speech are experienced.
- 0.25-0.40 Loss of motor functions. Marked decreases in responsiveness to stimuli and muscle control, impaired consciousness.
- 0.35-0.50 complete unconsciousness
- 0.450+  Death occurs by loss of involuntary muscle functioning such as breathing.


### Who is Required to be Tested?


Transportation workers with safety-sensitive duties in the industries of aviation, commercial motor vehicle, railroad, transit, and pipeline are all required to follow 49 CFR Part 40 of the U.S Department of Transportation's Alcohol Testing Procedures Rules. These workers are subject to random, reasonable suspicion, post-accident, and return-to-duty & follow-up testing. The rules prohibit safety-sensitive employees from performing any duties with a breath test results of 0.02 or greater.


<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->