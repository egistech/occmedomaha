---
layout: services
title:  "DOT Physical Exams"
group: services
---
The Department of Transportation requires all Commercial Drivers License (CDL) holders to receive periodic physical examinations, officially called a "Department of Transportation Medical Examination," to ensure the ability of the driver to safely operate a commercial vehicle.

### Who Needs a DOT Physical?

You are required to have a physical exam and carry a U.S. Department of Transportation (DOT) medical certificate if:

- You operate a motor vehicle with a gross vehicle weight rating (GVWR) or gross combination weight rating (GCWR) or gross vehicle weight (GVW) or gross combination weight (GCW) of 4,536 kilograms (10,001 pounds) or more in interstate commerce.
- You operate a motor vehicle designed or used to transport more than 15 passengers, including the driver, in interstate commerce.
- You operate a motor vehicle designed or used to transport between nine and 15 passengers, for direct compensation, beyond 75 air miles from your regular work-reporting location, in interstate commerce.
- You transport hazardous materials in quantities requiring placards, in interstate commerce.

The US Department of Transportation sets the following guidelines:

- CDL holders (Drivers) must have 20/40 correctable vision in each eye. Glasses or contact lenses are permitted.
- Drivers cannot be a diabetic on needle-injected insulin; diabetes controlled through a diet or oral medication is permitted.
- A driver's blood pressure must be under 140/90. Prescription medication to control blood pressure is permitted.
- Use of a Schedule 1 drug, amphetamine, narcotic or any other habit forming drug is not permitted.

If a driver has a current diagnosis of cardiac insufficiency, collapse, congestive cardiac failure or any other cardiovascular disease, he/she will be required to provide the Medical Examiner with a stress test (performed within the last 12 months) along with a release from the driver's physician stating that he/she can drive a commercial motor vehicle without restrictions.

The Department of Transportation also requires clearance for drivers that have been diagnosed with the following conditions: sleep apnea, recent back injury, recent major surgery, a current hernia, or have had recent workers' compensation claims.

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->

We have several providers that are registered with NRCME (National Registry of Certified Medical Examiners) who work at our numerous clinics. Please call the clinic closest to you to determine if there is an NRCME provider on site the day of your test.

### How Long is a Physical Exam Valid?

A DOT physical exam is valid for up to 24 months. The medical examiner may also issue a medical examiner's certificate for less than 24 months when an illness or condition requires monitoring of an employee's ongoing medical condition. (Example: use of hypertension medication.)

If the medical examiner finds that the person he/she examined is physically qualified to drive a commercial motor vehicle (CMV), the medical examiner will complete a Medical Examiner's Certificate and furnish copies to the driver and employer.

All CDL drivers must carry a current copy of a Medical Examination Certificate with them when they drive or perform safety sensitive functions.

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->