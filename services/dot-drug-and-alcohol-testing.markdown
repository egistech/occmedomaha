---
layout: services
title:  "DOT Drug & Alcohol Testing"
group: services
---
`**Photo – not the "injury" one, bottom one is ok, maybe a provider and a patient?**`

Family OccMed of Omaha employs certified collectors and technicians to administer DOT drug testing and breath alcohol (BAT) testing. We have several providers that are registered with NRCME (National Registry of Certified Medical Examiners) who work at our numerous clinics. Please call the clinic closest to you to determine if there is an NRCME provider on site the day of your test.

### History of DOT Testing

> The Department of Transportation (DOT) Drug Test policy is based on the notion of "Safety First" across all modes of transportation – on roads, rails, water, in the air, over land and underground. The cornerstone of this safety policy is to ensure transportation providers across all modes employ only those operators who are 100 percent drug and alcohol free.

> The DOT has worked hard to reduce the number of accidents and crashes directly related to drug and alcohol use. Federally regulated drug testing started in 1989. At the start of the DOT regulated drug testing program, 18 % of American truck drivers drug tested positive, with cocaine, marijuana or amphetamines in their urine. Since then with implementation of strict regulations the rate of positive drug tests has gone down tremendously. In 1998, 5% of pre-employment drug tests were positive for illicit drugs, and for those employees in random drug testing programs, the positive drug test rate was less than 2%. In the FAA program, the positive drug test rate fell to less than 1%.

> But nevertheless, some transportation workers do use illicit drugs, or abuse alcohol, despite serious efforts to deter them.

### U.S. D.O.T. Drug and Alcohol Testing Rules

Find out the complete information on U.S. D.O.T. Drug testing and alcohol testing rules for each of the transportation agencies including FAA, FMCSA, FRA, USCG, PHMSA and the FTA.

- Federal Aviation Administration (FAA) 14 CFR Part 120 details all Drug & Alcohol Testing Regulations for employers and employees in the aviation industry.
 - Click here to find out all about 14 CFR Part 120
- Federal Motor Carrier Safety Admin (FMCSA) 49 CFR Part 382 details all Drug & Alcohol Testing Regulations for carriers and commercial driver's license holders (CDL).
 - Click here to find out all about 49 CFR Part 382
- Federal Railroad Administration (FRA) 49 CFR Part 219 details all Drug & Alcohol Testing Regulations for employers and employees working in the railroad industry.
 - Click here to find out all about 49 CFR Part 219
- United States Coast Guard (USCG) 46 CFR Part 16 & 46 CFR Part 4 details all Drug & Alcohol Testing Regulations for employer and employees operating commercial vessels.
 - Click here to find out all about 46 CFR Part 16
 - Click here to find out all about 46 CFR Part 4
- Pipeline & Hazardous Materials Safety Administration (PHMSA) 49 CFR Part 199 details all Drug & Alcohol Testing Regulations for operators and employees working in the pipeline industry.
 - Click here to find out all about 49 CFR Part 199
- Federal Transit Administration (FTA) 49 CFR Part 655 details all Drug & Alcohol Testing Regulations for employers and employees working in the mass transit industry.
 - Click here to find out all about 49 CFR Part 655

`Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test`