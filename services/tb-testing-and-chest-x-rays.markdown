---
layout: services
title:  "TB Testing & Chest X-Rays"
group: services
---
### Tuberculosis Testing

<!--
[[LOOK UP TB STATS]]
-->

TB (Tuberculosis) is a community acquired contagious disease. The highest workplace exposure to Tuberculosis is in the healthcare setting. It can cause long-term illness and/or death if not properly diagnosed and treated. Most companies are required to provide yearly testing for TB. Testing can be completed either by a skin test or chest x-ray and any course of treatment will be determined at time of a positive result.

### Chest X-Rays

Most commonly used to rule out TB in the event that a TB skin test is deemed "positive", it is also beneficial for the screening of the chest and lungs in the events of: exposure to biological materials (such as Histoplasmosis), environmental hazards (such as asbestos) or situation (such as smoke inhalation). When an employee presents for a chest x-ray, they will also be examined by a medical provider to determine a course of treatment if necessary. 

All chest x-rays are sent for an over-read confirmation by a Board Certified Radiologist (BCR). 