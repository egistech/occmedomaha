---
layout: other
title:  "Hours & Locations"
---
### 168th & Maple

<div class="visible-md visible-lg">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2997.7231833896317!2d-96.17639280000003!3d41.29312869999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8793ee3eff9fe051%3A0x8243c8dd42cbb7cb!2s3830+N+167th+Ct!5e0!3m2!1sen!2sus!4v1395116147917" width="600" height="450" frameborder="0" style="border:0"></iframe>
</div>

<div class="visible-xs visible-sm">
  <img src="{{ root_url }}/images/map-168th-maple-compressed.png">
</div>

<address>
  <strong>168th &amp; Maple</strong><br>
  3830 North 167th Court<br>
  Omaha, NE 68116<br>
  <abbr title="Phone">P:</abbr> (402) 965-4000
</address>

### 88th & Maple

<div class="visible-md visible-lg">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2998.071326569779!2d-96.0492696!3d41.285552100000004!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x879392a1a857cacb%3A0x6d850643cacb2e19!2s8814+Maple+St!5e0!3m2!1sen!2sus!4v1395116182156" width="600" height="450" frameborder="0" style="border:0"></iframe>
</div>

<div class="visible-xs visible-sm">
  <img src="{{ root_url }}/images/map-88th-maple-compressed.png">
</div>

<address>
  <strong>88th &amp; Maple</strong><br>
  8814 Maple Street<br>
  Omaha, NE 68134<br>
  <abbr title="Phone">P:</abbr> (402) 343-0095
</address>

### 168th & Center

<div class="visible-md visible-lg">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3000.5491068780616!2d-96.17681805!3d41.23159545!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8793f10b86b1251f%3A0x2ee95871fde445bd!2s2921+S+168th+St!5e0!3m2!1sen!2sus!4v1395116205365" width="600" height="450" frameborder="0" style="border:0"></iframe>
</div>

<div class="visible-xs visible-sm">
  <img src="{{ root_url }}/images/map-168th-center-compressed.png">
</div>

<address>
  <strong>168th &amp; Center</strong><br>
  2921 S. 168th Street<br>
  Omaha, NE 68130<br>
  <abbr title="Phone">P:</abbr> (402) 334-2300
</address>

### Norfolk

<div class="visible-md visible-lg">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2963.801725001468!2d-97.43893355000002!3d42.02598569999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87903fd7ecd1ad09%3A0xa16fad20cfa83f37!2sUrgent+Care+of+Norfolk!5e0!3m2!1sen!2sus!4v1395116220268" width="600" height="450" frameborder="0" style="border:0"></iframe>
</div>

<div class="visible-xs visible-sm">
  <img src="{{ root_url }}/images/map-norfolk-compressed.png">
</div>

<address>
  <strong>Norfolk</strong><br>
  2024 Pasewalk Ave<br>
  Norfolk, NE 68701<br>
  <abbr title="Phone">P:</abbr> (402) 844-3830
</address>