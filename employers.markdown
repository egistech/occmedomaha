---
layout: other
title:  "Employers"
---
### What is Occupational Medicine?

Occupational Medicine is most simply, the area where companies, employee's health and medicine overlap. Family OccMed of Omaha is concerned with protecting the safety and health of today's workers, and the financial health of our client customers.

We commit to taking excellent care of your by providing services whether they need a physical, a drug test or need care for an injury or illness in our urgent care facilities.

We strive to increase productivity and reduce medical expenses by our doctors focusing on delivery prompt and high quality care and testing. This level of service helps injured employees recover and helps those that need testing to comply with industry standards quickly. This allows them to get back to work faster and easier. Your organization will realize enhanced productivity as well as a healthy bottom line with the services and approach used at Family OccMed of Omaha.

As experts in occupational medicine, we help your employees who are sick or injured on the job achieve the best possible recovery by:

- Determining if their illness or injury is work related.
- Identifying appropriate medical tests.
- Developing specialized treatment plans.
- Assisting in scheduling of rehabilitation services.

We can also help you prevent your employees from becoming sick or injured on the job by:

- Conducting pre-placement physicals
- Administering flu immunization

## [Breath Alcohol Testing](/services/breath-alcohol-testing.html)

### How Does Breath Alcohol Testing Work?

Anyone who consumes alcoholic beverages will exhale alcohol from their lungs. The alcohol concentration in the breath is in direct proportion to the concentration of alcohol in the blood that is making its way to the person's brain. The use of an Evidential Breath Tester (EBT) which measures the concentration of alcohol in a person's breath is the preferred method for testing alcohol levels in the body as it is a less invasive process than blood tests.

<!--
[[[FIND/TAKE A PICTURE OF OUR BREATH ALCOHOL TEST!!]]]
-->

### Alcohol Concentration in the Body and Impairment

- At 0.01-0.05 the ability to make good judgments become impaired.
- At 0.03-0.12 Sensory-motor control is impaired and slowed information processing is experiences.
- At 0.09-0.25 Reaction time is decreased, vision functions are reduced, drowsiness is experienced.
- At .09-.25Loss of critical judging abilities, impairment of perception, memory and comprehension is experienced.
- 0.18-0.30 Metal confusion, exaggerated emotional states, incoordination, slurred speech are experienced.
- 0.25-0.40 Loss of motor functions. Marked decreases in responsiveness to stimuli and muscle control, impaired consciousness.
- 0.35-0.50 complete unconsciousness
- 0.450+  Death occurs by loss of involuntary muscle functioning such as breathing.

### Who is Required to be Tested?

Transportation workers with safety-sensitive duties in the industries of aviation, commercial motor vehicle, railroad, transit, and pipeline are all required to follow 49 CFR Part 40 of the U.S Department of Transportation's Alcohol Testing Procedures Rules. These workers are subject to random, reasonable suspicion, post-accident, and return-to-duty & follow-up testing. The rules prohibit safety-sensitive employees from performing any duties with a breath test results of 0.02 or greater.

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->

## [DOT Physical Exams](/services/dot-physical-exams.html)

The Department of Transportation requires all Commercial Drivers License (CDL) holders to receive periodic physical examinations, officially called a "Department of Transportation Medical Examination," to ensure the ability of the driver to safely operate a commercial vehicle.

### Who Needs a DOT Physical?

You are required to have a physical exam and carry a U.S. Department of Transportation (DOT) medical certificate if:

- You operate a motor vehicle with a gross vehicle weight rating (GVWR) or gross combination weight rating (GCWR) or gross vehicle weight (GVW) or gross combination weight (GCW) of 4,536 kilograms (10,001 pounds) or more in interstate commerce.
- You operate a motor vehicle designed or used to transport more than 15 passengers, including the driver, in interstate commerce.
- You operate a motor vehicle designed or used to transport between nine and 15 passengers, for direct compensation, beyond 75 air miles from your regular work-reporting location, in interstate commerce.
- You transport hazardous materials in quantities requiring placards, in interstate commerce.

The US Department of Transportation sets the following guidelines:

- CDL holders (Drivers) must have 20/40 correctable vision in each eye. Glasses or contact lenses are permitted.
- Drivers cannot be a diabetic on needle-injected insulin; diabetes controlled through a diet or oral medication is permitted.
- A driver's blood pressure must be under 140/90. Prescription medication to control blood pressure is permitted.
- Use of a Schedule 1 drug, amphetamine, narcotic or any other habit forming drug is not permitted.

If a driver has a current diagnosis of cardiac insufficiency, collapse, congestive cardiac failure or any other cardiovascular disease, he/she will be required to provide the Medical Examiner with a stress test (performed within the last 12 months) along with a release from the driver's physician stating that he/she can drive a commercial motor vehicle without restrictions.

The Department of Transportation also requires clearance for drivers that have been diagnosed with the following conditions: sleep apnea, recent back injury, recent major surgery, a current hernia, or have had recent workers' compensation claims.

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->

We have several providers that are registered with NRCME (National Registry of Certified Medical Examiners) who work at our numerous clinics. Please call the clinic closest to you to determine if there is an NRCME provider on site the day of your test.

### How Long is a Physical Exam Valid?

A DOT physical exam is valid for up to 24 months. The medical examiner may also issue a medical examiner's certificate for less than 24 months when an illness or condition requires monitoring of an employee's ongoing medical condition. (Example: use of hypertension medication.)

If the medical examiner finds that the person he/she examined is physically qualified to drive a commercial motor vehicle (CMV), the medical examiner will complete a Medical Examiner's Certificate and furnish copies to the driver and employer.

All CDL drivers must carry a current copy of a Medical Examination Certificate with them when they drive or perform safety sensitive functions.

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->

## [DOT Testing](/services/dot-drug-and-alcohol-testing.html)

<!--
**Photo – not the "injury" one, bottom one is ok, maybe a provider and a patient?**
-->

Family OccMed of Omaha employs certified collectors and technicians to administer DOT drug testing and breath alcohol (BAT) testing. We have several providers that are registered with NRCME (National Registry of Certified Medical Examiners) who work at our numerous clinics. Please call the clinic closest to you to determine if there is an NRCME provider on site the day of your test.

### History of DOT Testing

> The Department of Transportation (DOT) Drug Test policy is based on the notion of "Safety First" across all modes of transportation – on roads, rails, water, in the air, over land and underground. The cornerstone of this safety policy is to ensure transportation providers across all modes employ only those operators who are 100 percent drug and alcohol free.

> The DOT has worked hard to reduce the number of accidents and crashes directly related to drug and alcohol use. Federally regulated drug testing started in 1989. At the start of the DOT regulated drug testing program, 18 % of American truck drivers drug tested positive, with cocaine, marijuana or amphetamines in their urine. Since then with implementation of strict regulations the rate of positive drug tests has gone down tremendously. In 1998, 5% of pre-employment drug tests were positive for illicit drugs, and for those employees in random drug testing programs, the positive drug test rate was less than 2%. In the FAA program, the positive drug test rate fell to less than 1%.

> But nevertheless, some transportation workers do use illicit drugs, or abuse alcohol, despite serious efforts to deter them.

### U.S. D.O.T. Drug and Alcohol Testing Rules

Find out the complete information on U.S. D.O.T. Drug testing and alcohol testing rules for each of the transportation agencies including FAA, FMCSA, FRA, USCG, PHMSA and the FTA.

- Federal Aviation Administration (FAA) 14 CFR Part 120 details all Drug & Alcohol Testing Regulations for employers and employees in the aviation industry.
 - Click here to find out all about 14 CFR Part 120
- Federal Motor Carrier Safety Admin (FMCSA) 49 CFR Part 382 details all Drug & Alcohol Testing Regulations for carriers and commercial driver's license holders (CDL).
 - Click here to find out all about 49 CFR Part 382
- Federal Railroad Administration (FRA) 49 CFR Part 219 details all Drug & Alcohol Testing Regulations for employers and employees working in the railroad industry.
 - Click here to find out all about 49 CFR Part 219
- United States Coast Guard (USCG) 46 CFR Part 16 & 46 CFR Part 4 details all Drug & Alcohol Testing Regulations for employer and employees operating commercial vessels.
 - Click here to find out all about 46 CFR Part 16
 - Click here to find out all about 46 CFR Part 4
- Pipeline & Hazardous Materials Safety Administration (PHMSA) 49 CFR Part 199 details all Drug & Alcohol Testing Regulations for operators and employees working in the pipeline industry.
 - Click here to find out all about 49 CFR Part 199
- Federal Transit Administration (FTA) 49 CFR Part 655 details all Drug & Alcohol Testing Regulations for employers and employees working in the mass transit industry.
 - Click here to find out all about 49 CFR Part 655

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->

## [Instant Drug Testing](/services/instant-and-lab-analysis-drug-testing-options.html)

Benefits of instant drug testing:

- Results within hours for negative results
- Increased worker productivity: reduced testing time allows for placement of employees into productive positions
- Reduced employment costs: decrease absenteeism due to substance abuse
- Shortened hiring cycle: eliminates 2-3 day delay in hiring prospective employees
- Reduced drug testing costs: lower "cost per test" because we have our own on-site lab for instant results

Instant Drug Testing Services include:

- Rapid Results – within hours for negative results
- All non-negative results are laboratory verified, within 2-3 working days and reviewed by a Certified MRO (Medical Review Officer)
- Proven laboratory accuracy
- FDA approved
- 5 Panel Test: Cocaine Metabolites, Marijuana Metabolities, Phenyclidine (PCP, "Angel Dust") , Amphetamines, Methamphetamines ("Speed"), Opiates (Codeine, Morphine, Heroin)

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->

## [Pre-Employment Drug Testing](/services/pre-employment-drug-testing.html)

A company with over 15 employees is subject to the **Americans with Disabilities Act**, a federal law that protects people with disabilities from discrimination. This Act prohibits pre-employment medical examinations before a conditional offer of employment has been made but a test to determine if an applicant is illegally using drugs is specifically exempted from the definition of what constitutes a medical examination. However pre-employment alcohol testing is considered a medical examination and can only be conducted after the candidate has been extended a conditional offer of employment.

Discrimination can be implied if an employer tests only certain applicants for a position. An employer cannot pick and choose which applicants for the same position will be tested. However within a company employment drug testing may be required for only certain positions. The company would have to be able to justify this in terms of the job requirements and a written drug policy.

### Why Do Pre-Employment Drug Testing?

The US Department of Labor has estimated that drug use in the workplace costs employers up to $100 billion dollars annually in lost work time, accidents, health care costs and workers compensation costs.

**Drug use affects your bottom line.** Workers who do drugs are more likely to change jobs or miss work. The good news is you can put programs in place to make sure your workplace is drug free. A good drug free program includes testing applicants prior to their hire date and performing random drug testing throughout their tenure with your company.

### What are Requirements for Pre-Employment Drug Testing ?

Generally these requirements for pre-employment drug screening are followed by employers:

- Employers have a written drug testing policy that requires job applicants to be drug-free.
- Written notice of testing is given before the applicant may be tested. Many drug and alcohol testing laws require that job applicants be notified in advance that they may be tested and under what conditions.
- Written notice is given to the applicant that employment drug screening is required before hiring. This may be done through the employment application form or on a specific form given out at the first interview.
- The written notice details the type of drug testing that will be carried out and lists the over-the-counter medications that may produce a positive result.
- The same testing program should be implemented for all applicants in a particular category or there could be implications of discrimination.

**Photo – better photo of outside of clinic**

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->

## [Pre-Employment Physical Exams](/services/pre-employment-physical-exams.html)

<!--
Photo needs to be of an examiner with a patient (maybe vision testing?)
-->

Pre-Employment Physical Examinations are performed to determine if an applicant is physically capable of meeting the essential functions of the job, while maintaining the examinee's privacy and medical confidentiality. A well designed physical exam program results in fewer injuries, lower costs and a better match of employees to their jobs.

The exam is performed with attention to the job duties, including physical requirements and potential exposures to hazardous materials. The content of the examination depends on the job and the work-site assessment. For example, jobs that require heavy lifting may require a back assessment to determine if the employee is able to perform the functions of the job. Those involved in interstate trucking usually require a physical exam and a urinary drug test.

To avoid errors in either the content or the context of the examination, Family OccMed of Omaha uses standard protocols to which the employer and the examining provider both agree. Following the examination, the medical provider gives a written opinion about the person's suitability to perform the job to ensure maximized productivity while reducing the potential for injury to themselves or others.

### A Medical Examination Typically Includes:

Family OccMed of Omaha providers will carefully review your medical and occupational history before proceeding with the physical examination.

- Complete occupational/medical history
- Musculoskeletal examination
- Evaluation of the respiratory system
- Gastrointestinal examination
- Examination of head, eyes, ears, nose, and throat
- Skin and lymphatic examination
- Neurological evaluation
- Evaluation of the cardiovascular system
- Visual acuity and vital signs, including blood pressure, urine dip test
- For a convenient, walk-in pre-employment physical, contact Family OccMed of Omaha.

<!--
Link to The HOW (process) for this test ….. Link to the necessary FORM for this test…And PRICE for the Test
-->

## [Workman's Compensation (Work Comp)](/services/workmans-comp.html)

Family OccMed of Omaha provides treatment for work related illness and injuries, post-accident drug and/or alcohol testing and on-going monitoring of an employee's progress during their recovery process. We work closely with employers to provide appropriate restrictions that allow Companies to provide alternative work duties, including "light duty".

### Workplace Illness or Injury:

When an employee presents for care, we will need authorization from you 
-->LINK TO AUTHORIZATION FORM
--> that you are aware of this illness or injury and the patient is a valid employee of yours. The employee you send needs to know the name and number of the person at your company to call for us to talk to.

When we call you, some of the questions we will ask are:

- For verbal authorization of care
- Work comp insurance carrier and Policy number 
  - We need to know this in order to file the claim properly without incurring more expenses.
- Do you want the employee tested for drugs or alcohol?
  - Family OccMed of Omaha has the ability to test the employee for drugs and/or alcohol at the time of treatment. To streamline the process, please let us know as soon as possible that a post-accident drug and/or alcohol test is needed.

If you have sent an employee to us and they are on their way, you can speed up the process by giving us a call at ______________ / _________________ / __________________ so that we can authorize the information and begin the treatment process as soon as the patient arrives.

##### Coordination of Care

Family OccMed of Omaha provides on-going monitoring of an employee's progress during their recovery process. We work closely with physical therapists, orthopaedic physicians, and other healthcare modalities to facilitate the best possible outcome for your employee.

<!--
ADD LINK TO THE FORM ABOUT WORKERS' COMP
-->

## [TB Testing and Chest X-Rays](/services/tb-testing-and-chest-x-rays.html)

### Tuberculosis Testing

TB (Tuberculosis) is a community acquired contagious disease. [[LOOK UP TB STATS]] The highest workplace exposure to Tuberculosis is in the healthcare setting. It can cause long-term illness and/or death if not properly diagnosed and treated. Most companies are required to provide yearly testing for TB. Testing can be completed either by a skin test or chest x-ray and any course of treatment will be determined at time of a positive result.

### Chest X-Rays

Most commonly used to rule out TB in the event that a TB skin test is deemed "positive", it is also beneficial for the screening of the chest and lungs in the events of: exposure to biological materials (such as Histoplasmosis), environmental hazards (such as asbestos) or situation (such as smoke inhalation). When an employee presents for a chest x-ray, they will also be examined by a medical provider to determine a course of treatment if necessary. 

All chest x-rays are sent for an over-read confirmation by a Board Certified Radiologist (BCR). 

<!--
DEFINITIONS:
[[Here's where we will put definitions to OccMed words like……and what it is/why they should do it]]
Random Pool (Consortium)
DOT Drug Screen
Lab Analysis
Instant Drug Test
Physicals
DOT/Pre-Employment Testing
-->