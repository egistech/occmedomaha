---
layout: other
title:  "About"
---
### Who is Family OccMed of Omaha?

We are a group of highly skilled providers and technicians focused on YOU!  Customer service and care are our top priority.  We provide services on a walk-in basis, in our several conveniently located clinics with extended hours including evenings, weekends and holidays.  At Family OccMed of Omaha, you and your employees are not just a number, you are treated as family.  We are constantly learning and growing and look forward to working with you to create a comprehensive and beneficial corporate wellness program for your company.

### Who is our customer?

Our customer is both the employer and the employee.  We strive to work with established corporations who genuinely care not only for following government regulations but also for the continued health and well-being of their employees.  Our goal is to care for employees at the time of injury, illness or testing as well as reducing costs and loss of employee time and production for the employer.  We envision continued care for the employee by coordinating recovery services as well as providing convenient hours and locations for their urgent care needs.

<center><h4>Family OccMed of Omaha is here to deliver quick, affordable results for you and your employees.</h4></center>