---
layout: other
title:  "Frequently Asked Questions"
---
### What is Occupational Medicine?

Occupational Medicine is the branch of clinical medicine most active in the field of Occupational Health. Its principal role is the provision of health advice to organizations and individuals to ensure that the highest standards of Health and Safety at Work can be achieved and maintained. Occupational Physicians must have a wide knowledge of clinical medicine and be competent in a number of important areas.

### Can I use any of the Family OccMed of Omaha clinics?

Yes, many of our client employers use all of our clinics so their employees can choose the clinic closest to home or work.  Please call the clinic closest to you to be certain that there is a ________(board certified…trained….whatever the term is) Medical Examiner on staff at that time.

### Do my employees need appointments?

Appointments are preferred, but not required. Our clinic staff will assist you and your employees with making appointments and also explain "walk-in" access when needed. No appointments are taken for Drug screens.

### How do I start using Family OccMed of Omaha?

That's easy. Call us today to discuss your company's needs. Our staff will explain our services, clinics and fees. Once you have an account set up, you can begin using our services.  
-->LINK to credit application, LINK to Forms…
-->

### Why is an Occupational Health program important to an employer's bottom line? 

Healthy workers are more productive, and work-related injuries are very expensive.  The direct costs of injuries that are easy to measure, like the cost of medical care and insurance costs, are small when compared to indirect costs associated with workplace disruption, retraining, lost productivity, etc.  When injuries do occur, they need to be managed well to return the injured worker to a state of health and productivity as soon as possible.  A skilled Occupational Medicine clinical practice will work with the worker, employer, workers compensation insurer, and the medical system to help the injured worker regain his or her functional abilities and return to work. 

### What services does Family OccMed of Omaha offer?

<!--
LIST all Services, including "Urgent Care"
-->
 
### If I don't want to put an injury under the workers compensation insurance, can I put it under my own private health insurance?

<!--
??????
-->
 
### Do I need to make an appointment for a drug screen?

No. Clients needing a drug screen are seen on a first come, first serve basis.
 
### How long does it take for drug screen results to come back?

Negative results are reported to your employer in 24-36 hours. An MRO (Medical Review Officer) will contact you to discuss any positive results usually within 2-3 days following collection. Results are reported to your employer immediately following the MRO's discussion with you.
 
### How do I contact the Occupational Medicine Clinic?

<!--
CONTACT INFO, link to LOCATIONS
-->
 
### What if my employee gets hurt after hours?

Our urgent care facilities are open until ________________________________. (locations and hours)

### What is the recent change in DOT testing?

Starting January 30, 2012 and no later than January 30, 2014, all CDL holders must provide information to their SDLA regarding the type of commercial motor vehicle operation they drive in or expect to drive in with their CDL. Drivers operating in certain types of commerce will be required to submit a current medical examiner's certificate to their SDLA to obtain a "certified" medical status as part of their driving record. CDL holders required to have a "certified" medical status who fail to provide and keep up-to-date their medical examiner's certificate with their SDLA will become "not-certified" and they may lose their CDL.
 
#### What is changing?

State driver licensing agencies (SDLAs) will be adding your medical certification status and the information on your medical examiner's certificate to your Commercial driver's license system (CDLIS) record.

#### When does this change start?

This change starts on January 30, 2012. What is not changing? The driver physical qualification requirements are not changing.
 
#### What are CDL holders required to do? 

1. You must determine what type of commerce you operate in. You must certify to your SDLA to one of the four types of commerce you operate in as listed below,
  - Interstate non-excepted: You are an Interstate non-excepted driver and must meet the Federal DOT medical card requirements (e.g. – you are "not excepted").
  - Interstate excepted: You are an Interstate excepted driver and do not have to meet the Federal DOT medical card requirements.
  - Intrastate non-excepted: You are an Intrastate non-excepted driver and are required to meet the medical requirements for your State.
  - Intrastate excepted: You are an Intrastate excepted driver and do not have to meet the medical requirements for your State.
2. If you are subject to the DOT medical card requirements, provide a copy of each new DOT medical card to your SDLA prior to the expiration of the current DOT medical card.
 
### How quick is your turn around on drug screenings?

Industry standard is 24 hours for test that have been forwarded to the lab for testing. Family OccMed of Omaha also offers a rapid read drug testing option that gives results as the employee is in the clinic. Positive tests are forwarded to the appropriate lab for confirmation testing.
 
### I am an employer, how can I receive my drug screen results?

????????????????????
 
### What can I expect as a new patient?

?????????????????????????
 
### What kind of services do your clinics provide?

Family OccMed of Omaha is a full Occupational Medicine program that has the ability to provide a variety of employee healthcare options for you as an employer, as well as workers injury and illness care. See our [services](/services/)
 
### When am I entitled to Workers' Compensation?

Workers' compensation insurance is a no-fault insurance policy, which provides wage loss and medical benefits to workers with a job-related injury or disease. Nearly every worker is protected by workers' compensation insurance, as state law requires most employers to have workers' compensation insurance.

A job-related injury or disease can be defined as: An injury or illness that occurred while the employee was "Working within the usual and customary confines of his/her usual position."

If you obtain an injury while at work, information will be sent to the insurance surety. Your claim will be assigned to a claims manager who will review all the information regarding your injury or illness and, based on appropriate guidelines, will determine if the claim is accepted and allowed.
 
### Can I expect immediate treatment if I go to the Urgent Care, how long can I expect to wait?

Yes. Within Family OccMed of Omaha you will be evaluated by our well trained physicians and mid-level providers who will initiate immediate care and assist in opening a worker's compensation claim for you.

Wait times in an Urgent Care setting can vary on a minute to minute basis. As patients present to the clinic, they are evaluated for the severity of the reason they are coming in. If another patient has a more severe situation than you, you may be required to wait.
 
### What is the difference between "Urgent Care" and "Emergency Room Care?"

Cost. An Emergency Room visit, for the same injury, can cost as much as five times that of the care received in an Urgent Care setting.

Having said that; an Emergency Room is open 24 hours a day, where an Urgent Care clinic will close around 9:00 – 10:00 at night.

It is important to remember that workers compensation insurance does have a cost, part of which is paid directly by the employee. As claims are filed, the cost increases, and a portion of those increases will be directed to the employees of the business.
 
### What are your hours of operation? 

Take a look at our [hours & locations page](/hours-and-locations.html)
 
### What if I am on medications that could possibly make my drug screens positive?

Drug screens fall primarily under two categories; DOT (NIDA) and Non-DOT (Non-NIDA).

DOT Drug screens: DOT drug screens will use a Medical Revue Officer (MRO) to review all positive results. The MRO will contact you, at which time you are able to explain why the medications are present.

Non-DOT Drug screens: Non- DOT drug screens are not mandated the way DOT drug screens are. Because of this, non-DOT drug screen results may go to and MRO for review, but may not. If the results go to the MRO the process will be the same as the DOT drug screen, if your employer does not use an MRO you will have to discuss the results with an elected employee representative who will follow the guidelines set by your employer.
 
### What time frame should I expect when I come in to your office/clinic?

When you come to the clinic for your visit you should expect to be at the clinic for approximately 1 hour.
 
### Do I need to bring anything with me for my drug screen?

Picture ID. The drug screen collector is required to confirm that the person present is the person required to do the drug screen prior to initiating the collection.
 
### As an employer, how can I sign my company up for occupational medicine services?
 
### What should I do if I am injured on the job?

1. Report your injury or exposure to your supervisor.
2. Got to Central Washington Occupational Medicine for expert medical care.
3. Stay in touch with your employer.
4. Communicate with your health care provider and work closely with your claims manager.
 
### How do I prepare for my appointment/testing?

- Arrive 15 minutes prior to your appointment time
- Bring photo identification
- Print and complete the patient forms that are needed for the services you are receiving.  The forms are listed by type of service here. 
-->LINK TO FORMS
-->
- Bring (or wear) your glasses or contacts if you wear corrective lenses of any kind for any purpose.
- If your visit includes any of the following exams/components, please prepare accordingly:
  - Lifting Test/PC – Wear comfortable clothing and closed toed shoes
  - Blood work – Drink plenty of water the day of your visit, adequate hydration makes obtaining your sample easier
  - Respirator Fit Test – Be sure that any facial hair complies with OSHA regulations for respirator fit testing which state "The test shall not be conducted if there is any hair growth between the skin and the face piece sealing surface, such as stubble beard growth, beard, mustache or sideburns which cross the respirator sealing surface.  Any type of apparel which interferes with a satisfactory fit shall be altered or removed."
 
### What is involved in a pre-placement exam?

The exam components vary to some degree based on your company's requests.  Most exams include: Height, weight, vital signs (blood pressure, pulse, respiratory rate, temperature), vision (near, far, peripheral, color), hearing (either as a whisper test or audiogram if requested), urine dipstick (this is not a drug screen), urine drug screen (if it is requested by your company), physical exam (this is performed by one of our Physicians to verify that there are no health conditions that would limit your ability to safely do your job.)
 
### Who will provide my medical care?

EXAMPLE --Pulse Occupational Medicine and Immediate Care centers are staffed by board-certified physicians who are skilled in the treatment of non-life-threatening injuries and illnesses. Many physicians practice in local emergency rooms. All of our providers—physicians, physician assistants, nurses and radiological technicians—are dedicated to providing excellent medical care in a compassionate, respectful manner.

### A DOT physical is valid for how long?

If the results of the exam are normal, an exam is required once every two years. If medical issues are involved, DOT exams may be required in less than two years (e.g., once every 6 months).

### When can I come in for a drug test?

<!--
??????????????????
-->

### How long does it take for an employer to receive drug screen results?

Results are usually available within 24-36 hours for lab-based collections. We also offer instant 5-panel drug testing with results in 20 minutes.

### Why is "light-duty" a good idea for workplace injury?

One aspect of a company's overall cost management program is the development of a transitional RETURN-TO-WORK program, i.e., a "light duty" or "modified duty" program for injured employees.  A transitional return-to-work program can range from modification of the employee's existing job, to "light duty" position, to development of temporary positions or to "work hardening" programs . Such programs can be as diverse and creative as an employer wants it to be.  Transitional return-to-work programs have proven beneficial to both the employee and the company. 
 
Companies that have implemented light duty programs counter that an aggressive approach to injury management makes perfect business sense. Insurance industry data indicates that companies with structured light duty programs return their employees to work in some capacity 50 percent faster than employers with unstructured programs. In addition, data also indicates that employees reach maximum medical improvement three times as fast in these structured programs.
 
A "Light Duty" program:

- Lowers costs for your workers' compensation insurance carrier, thus lowering costs to you.
- Saves time and dollars on recruitment and training of new employees by retaining experienced and proven employees.
- Lowers medical and legal costs. Studies have shown the quicker you get injured workers back to work, the quicker they are to rehabilitate and the less likely they are to consult an attorney or seek additional medical treatment.
- Maintains contact with the injured worker.
- Decreases recovery time.

If you as the employer do not offer light duty or a modified position to an injured worker, that worker will remain on temporary total compensation until s/he reaches a point of medical stability.
 
### Is the employer required to provide light duty?

No, the employer is not required to provide light duty; however, as mentioned above, there are many advantages to the employer and employee by offering light duty.
 
### If I bring an employee back on light duty, do I have to pay them the same rate of pay?

No, you do not have to pay the employee the same rate of pay; however, the insurance company would pay 66 2/3 the difference between what the employee was earning at the time of the injury and what the employee is now earning on light duty.
 
### What is Workers' Compensation?

Workers' compensation is a wage replacement and medical care program for a worker whose injury or illness is work related.
 
### What are the employer's responsibilities under Workers' Compensation?

Posting notice. Employers are required to post in conspicuous places typewritten or printed notices stating that they have complied with all the rules and regulations securing compensation to employees and their dependents (notices are available free of charge at the Labor Commission). The notices should state the name of the insurance carrier, the phone number, and steps to report an industrial claim.

Reporting Industrial Accidents. A worker has up to 180 days to report an injury or worker-related illness. Once the injury/illness has been reported to the employer, the employer has seven days, to file the "Employer's First Report of Injury or Illness" with the Labor Commission, and submit a copy of the report to their Workers' Compensation Insurance Carrier and the injured worker.
